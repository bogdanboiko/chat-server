
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class Server {

    private static JSONArray mainBase;
    private ConcurrentHashMap<String, ObjectOutputStream> clientCommunication = new ConcurrentHashMap<>();
    private static File usersBase;
    private static   ServerSocket serverSocket;
    private final int PORT = 12121;

    public static void main(String[] args) {
            System.out.println("To stop server write STOP");
            Thread serverThread = new Thread(() -> {
                try {
                    new Server().start();
                } catch (IOException e) {
                    saveData();
                }
            });
            serverThread.start();
            while (true) {
                if (new Scanner(System.in).nextLine().equals("STOP")) {
                    try {
                        serverSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                } else {
                    System.out.println("Write correctly");
                }
            }
    }

    public void start() throws IOException {
        usersBase = new File("UsersBase.json");

        if (!usersBase.exists())
            createBase(usersBase);

        BufferedReader fileReader = new BufferedReader(new FileReader(usersBase));
        readBase(fileReader);
        fileReader.close();
        serverSocket = new ServerSocket(PORT);

        while (true) {
            System.out.println("In the beginning of loop");
            Socket socket = serverSocket.accept();

            new Thread(() -> {
                System.out.println("Got user");

                try {
                    userThreadRealisation(socket);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    private static void createBase(File file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[]" + '\n');
        fileWriter.flush();
        fileWriter.close();
    }

    private boolean checkUsername(String username) {
        for (int i = 0; i < mainBase.length(); i++) {
            if (username.equals(mainBase.getJSONObject(i).getString("Username"))) {
                return true;
            }
        }
        System.out.println("username is free");
        return false;
    }

    private boolean checkPassword(String password) {
        System.out.println("User password" + password);

        for (int i = 0; i < mainBase.length(); i++) {
            if (password.equals(mainBase.getJSONObject(i).getString("Password"))) {
                return true;
            }
        }

        System.out.println("password is not correct");
        return false;
    }

    private void readBase(BufferedReader fileReader) throws IOException {
        String buffer;
        StringBuilder res = new StringBuilder();

        while ((buffer = fileReader.readLine()) != null) {
            res.append(buffer);
        }

        mainBase = new JSONArray(res.toString());
    }

    private void userThreadRealisation(Socket socket) throws IOException, ClassNotFoundException {
        ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());

        while (true) {
            System.out.println("Wait client to choose");
            String clientMessage = (String) reader.readObject();
            System.out.println("Got flag");
            JSONObject user = new JSONObject(clientMessage);
            System.out.println(user.getString("Flag"));

            switch (user.getString("Flag")) {
                case "Register":
                    if (!checkUsername(user.getString("Username"))) {
                        writer.writeObject(new JSONObject().put("IsAccepted", true).toString());
                        System.out.println("Registration passed");
                        writer.flush();
                        clientMessage = reader.readObject().toString();
                        user = new JSONObject(clientMessage);
                        mainBase.put(user);
                        System.out.println(mainBase.toString());
                        saveData();
                    } else {
                        writer.writeObject(new JSONObject().put("IsAccepted", false).toString());
                        System.out.println("Registration failed");
                        writer.flush();
                    }
                    socket.close();
                    break;
                case "Login":
                    writer.writeObject(new JSONObject().put("IsAccepted",
                            checkUsername(user.getString("Username")) &&
                                    checkPassword(user.getString("Password"))).toString());
                    System.out.println("Login failed");
                    writer.flush();
                    socket.close();
                    break;
                case "Chatting":
                    String username = user.getString("Username");
                    clientCommunication.put(username, writer);
                    break;
                case "Mailing":
                    String userMessage = user.getString("Message");

                    for (String key : clientCommunication.keySet()) {
                        System.out.println(key);
                        try {
                            clientCommunication.get(key).writeObject(new JSONObject().put("Message", userMessage).toString());
                            clientCommunication.get(key).flush();
                        } catch (IOException e) {
                            clientCommunication.remove(key);
                        }
                    }
                    break;
            }
        }
    }

    public static void saveData() {
        try {
            FileWriter writer = new FileWriter(usersBase);
            writer.write(mainBase.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
